<?php
session_start();
require_once __DIR__."/vendor/autoload.php";
include("./Connect/connect.php");
$app = new Silex\Application();
$app['debug']=true;

//Attribution des variables de session
$lvl=(isset($_SESSION['level']))?(int) $_SESSION['level']:1;
$id=(isset($_SESSION['id']))?(int) $_SESSION['id']:0;
$pseudo=(isset($_SESSION['pseudo']))?$_SESSION['pseudo']:'';

//On inclue les 2 pages restantes
include("functions.php");
include("constants.php");



$totaldesmessages = 0;
$categorie = NULL;
$query=$db->prepare('SELECT cat_id, cat_nom,
forum_forum.forum_id, forum_name, forum_desc, forum_post, forum_topic, auth_view, forum_topic.topic_id,  forum_topic.topic_post, post_id, post_time, post_createur, membre_pseudo,
membre_id
FROM forum_categorie
LEFT JOIN forum_forum ON forum_categorie.cat_id = forum_forum.forum_cat_id
LEFT JOIN forum_post ON forum_post.post_id = forum_forum.forum_last_post_id
LEFT JOIN forum_topic ON forum_topic.topic_id = forum_post.topic_id
LEFT JOIN forum_membres ON forum_membres.membre_id = forum_post.post_createur
WHERE auth_view <= :lvl
ORDER BY cat_ordre, forum_ordre DESC');
$query->bindValue(':lvl',$lvl,PDO::PARAM_INT);
$query->execute();

// while($data = $query->fetch()){
// echo "coucou";
// }
// exit();
$test=1;

$app->get('/',function () use ($query,$totaldesmessages,$categorie) {
  ob_start();
  require "templates/accueil.php";
  $test = ob_get_clean();
  return $test;
});

$app->get('/connect',function () use ($test) {
  ob_start();
  require "templates/connexion.php";
  $test = ob_get_clean();
  return $test;
});

$app->get('/disconnect',function () use ($test) {
  ob_start();
  require "templates/deconnexion.php";
  $test = ob_get_clean();
  return $test;
});

$app->run();
?>
