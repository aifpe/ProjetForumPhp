<?php
require_once("connectMaison.php");
$dsn="mysql:dbname=".BASEMAISON.";host=".SERVERMAISON.";charset=utf8";
try {
    $db = new PDO($dsn, USERMAISON,PASSWDMAISON);
}
catch (Exception $e) {
    try {
        require_once("connectEcole.php");
        $dsn = "mysql:dbname=" . BASEECOLE . ";host=" . SERVERECOLE . ";charset=utf8";
        $db = new PDO($dsn, USERECOLE, PASSWDECOLE);
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
}
?>
